/* jshint node:true */
'use strict';
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var gft = require('gulp-file-tree');

gulp.task('styles', function () {
  return gulp.src('app/styles/main.scss')
    .pipe($.sass({
      style: 'expanded',
      errLogToConsole: true
    }))
    .pipe($.autoprefixer({
        browsers: ['> 1%','last 3 versions']
      }))
    .pipe(gulp.dest('.tmp/styles'));
});

gulp.task('jshint', function () {
  return gulp.src('app/scripts/**/*.js')
    .pipe($.jshint())
    .pipe($.jshint.reporter('jshint-stylish'))
    //.pipe($.jshint.reporter('fail'));
});

gulp.task('html', ['styles'], function () {
  var lazypipe = require('lazypipe');
  var cssChannel = lazypipe()
    .pipe($.csso)
    .pipe($.replace, 'bower_components/bootstrap-sass-official/assets/fonts/bootstrap','fonts');
  var assets = $.useref.assets({searchPath: '{.tmp,app}'});

  return gulp.src(['app/*.html', '!app/index.html'])
    .pipe(assets)
    .pipe($.if('*.js', $.uglify()))
    .pipe($.if('*.css', cssChannel()))
    .pipe(assets.restore())
    .pipe($.useref())
    .pipe($.if('*.html', $.minifyHtml({conditionals: true, loose: true})))
    .pipe(gulp.dest('dist'));
});

gulp.task('images', function () {
  return gulp.src('app/images/**/*')
    .pipe($.cache($.imagemin({
        optimizationLevel: 3,
        progressive: true,
        interlaced: true
    })))
    .pipe(gulp.dest('dist/images'))
    .pipe($.size({showFiles: false}));
});

gulp.task('fonts', function () {
  return gulp.src(require('main-bower-files')().concat('app/fonts/**/*'))
    .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
    .pipe($.flatten())
    .pipe(gulp.dest('dist/fonts'));
});

gulp.task('extras', function () {
  return gulp.src([
    'app/*.*',
    '!app/*.html',
    'node_modules/apache-server-configs/dist/.htaccess'
  ], {
    dot: true
  }).pipe(gulp.dest('dist'));
});

gulp.task('tree', function () {
  return gulp.src(['app/*.html'])
      .pipe(gft())
      .pipe(gulp.dest('./.tmp'))
      .pipe($.size());
});

gulp.task('clean', require('del').bind(null, ['.tmp', 'dist']));

// inject bower components
gulp.task('wiredep', function () {
  var wiredep = require('wiredep').stream;

  gulp.src('app/styles/*.scss')
    .pipe(wiredep())
    .pipe(gulp.dest('app/styles'));

  gulp.src('app/*.html')
    .pipe(wiredep({exclude: ['bootstrap-sass-official']}))
    .pipe(gulp.dest('app'));
});

gulp.task('build', ['jshint', 'html', 'images', 'fonts', 'extras'], function () {
  return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('default', ['clean'], function () {
  gulp.start('build');
});

gulp.task('watch', ['styles'] ,function () {
  gulp.watch('app/styles/**/*.scss', ['styles']);
  gulp.watch('app/scripts/**/*.js', ['scripts']);
  gulp.watch('app/images/**/*', ['images']);
  gulp.watch('bower.json', ['wiredep']);
  gulp.watch('app/*.html', ['tree']);
});

var argv = require('yargs').argv,
    browser = 'default';

function browserSyncInit(baseDir, startPath, argv) {
  var browserSync = require('browser-sync'),
      files = ['.tmp/styles/**/*.css',
        'app/images/**/*',
        'app/*.html',
        'app/scripts/**/*.js'];

  if (argv.chrome == true) {
    browser = 'google chrome';
  } else if (argv.firefox == true) {
    browser = 'firefox';
  } else if (argv.safari == true) {
    browser = 'safari';
  } else if (argv.ie11 == true) {
    browser = 'internet explorer 11';
  } else if (argv.ie10 == true) {
    browser = 'internet explorer 10';
  } else if (argv.ie9 == true) {
    browser = 'internet explorer 9';
  } else if (argv.ie8 == true) {
    browser = 'internet explorer 8';
  } else if (argv.mac == true) {
    browser = ['google chrome', 'firefox', 'safari'];
  } else if (argv.ie == true) {
    browser = ['internet explorer 8', 'internet explorer 9', 'internet explorer 10', 'internet explorer 11'];
  } else {
    browser = 'default';
  }

  var routes = {
    '/bower_components': 'bower_components'
  };

  browserSync.instance = browserSync.init(files, {
    startPath: startPath,
    server: {
      baseDir: baseDir,
      routes: routes
    },
    ghostMode: true,
    browser: browser,
    online: true,
    xip: true, // online required - useful for services such as Typekit configure domain *.xip.io
    open: "external", // online required - opens IP adress
    notify: false
  });
}

gulp.task('serve', ['tree','watch'], function () {
  browserSyncInit(['app','.tmp'],'index.html', argv);
});

gulp.task('serve:dist', ['build'], function () {
  browserSyncInit('dist', 'home.html', argv);
});