# Boiler #

A HTML5 boilerplate kickstarter for modern webapps.

### What included ?###

* GulpJS workflow
* Bower package manager
* HTML5 Boilerplate (Customized)
* Node Sass
* Bootstrap 3.3.1
* Modernizr 2.8.1
* Font Awesome 4.2.0
* And many more extra's

### How do I get set up? ###

Install the development dependencies: 
```
$ npm install
```

Install the required vendor packages:
```
$ bower install
```